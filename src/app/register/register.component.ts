import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import USER from '../USER';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  form = new FormGroup({
    first_name :  new FormControl('',[Validators.required,]),
    last_name: new FormControl('',[Validators.required]),
    phoneNumber: new FormControl('',[Validators.required,Validators.minLength(10),Validators.maxLength(10)]),
    age: new FormControl('',[Validators.required]),
    gender: new FormControl('',[Validators.required]),
    date: new FormControl('',[Validators.required]),
    postal: new FormControl('',[Validators.required]),
    address: new FormControl('',[Validators.required]),
    district: new FormControl('',[Validators.required]),
    region: new FormControl('',[Validators.required]),
    state: new FormControl('',[Validators.required])
  })

  constructor(public router: Router,private http: HttpClient) { }

  ngOnInit(): void {
  }

  save(){
    var x:any = document.getElementById('image');
    var img = x['value'];
    USER.userDetails = {...this.form.value};
    if(img.length==0 || img==null) alert('please upload your profile picture')
    else this.router.navigateByUrl('/weather');
  }

  onPostalCodeChange(event: any){
    var x = event.target.value;
    if(x.length == 6){
      var link = `https://api.postalpincode.in/pincode/${x}`
      this.http.get(link).subscribe(da =>{
        var data:any = da;
        if(data[0]['Status']==="Success"){
          var district = data[0]['PostOffice'][0]['District'];
          var region = data[0]['PostOffice'][0]['Region'];
          var state = data[0]['PostOffice'][0]['State'];
          this.form.patchValue({district: district, region: region, state: state})
        }
        else alert("please enter valid pin code");
      });
    }
  }

  calAge(){
    let timeDiff = Math.abs(Date.now() - Date.parse(this.form.value.date));
    let age = Math.floor((timeDiff / (1000 * 3600 * 24))/365.25);
    this.form.patchValue({age: age})
  }



}
