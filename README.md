# Project

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.6.

This is deployed at [https://sample-a725e.web.app](https://sample-a725e.web.app)

## Framework and libraries used

##### Framework: Angular 11
##### libraries: Material UI
##### languages: HTML, typescript, scss
##### API: openweathermap, postalpincode api

## Sreenshots

![ss1](https://firebasestorage.googleapis.com/v0/b/sample-a725e.appspot.com/o/ss1.png?alt=media&token=46668646-0825-4106-ab61-c11588a328a0)
![ss2](https://firebasestorage.googleapis.com/v0/b/sample-a725e.appspot.com/o/ss2.png?alt=media&token=0668e1d3-4da1-4ed1-b401-a557c89dbe68)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
