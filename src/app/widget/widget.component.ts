import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import USER from '../USER';
import { Router } from '@angular/router';
@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.scss']
})
export class WidgetComponent implements OnInit {
  user:any = {};
  weather:any = {};
  d = new Date();
  months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  date = this.d.getDate()+"/"+this.months[this.d.getMonth()]+"/"+this.d.getFullYear();
  time = this.d.getHours()+" :  "+this.d.getMinutes();
  day = "day";
  constructor(private http: HttpClient,private router: Router) {
    this.user = USER.userDetails;
    if(this.user['district']==undefined){
      this.router.navigateByUrl('');
      return;
    }
    var link = `https://api.openweathermap.org/data/2.5/weather?q=${this.user['district']}&appid=4d8fb5b93d4af21d66a2948710284366&units=metric`;
    this.http.get(link).subscribe(d =>{
      var data:any = d;
      this.weather['name'] = data['name'];
      this.day = (data['weather'][0]['icon'].charAt(2)=='d')?"day":"night";
      this.weather['temp'] = data['main']['temp'];
      this.weather['max'] = data['main']['temp_max'];
      this.weather['min'] = data['main']['temp_min'];
      this.weather['main'] = data['weather'][0]['main'];
      this.weather['desc'] = data['weather'][0]['description'];
      this.weather['icon'] = `http://openweathermap.org/img/wn/${data['weather']['icon']}@2x.png`;
    })
  }

  ngOnInit(): void {

  }

}
